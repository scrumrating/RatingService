﻿using System;
using System.Collections.Generic;
using System.Text;

namespace RatingService.Data.Entity
{
    public class Rating
    {
        /// <summary>
        /// Инициализирует новый экземпляр класса <see cref="Rating"/>.
        /// </summary>
        /// <param name="id">Идентификатор оценки</param>
        /// <param name="studentId">Идентификатор студента</param>
        /// <param name="linkKind">Тип оценки (1 - за Урок, 2 - за ДЗ)</param>
        /// <param name="linkId">Ссылка на "Оценка за ДЗ" или "Оценка за урок"</param>
        /// <param name="value">Значение оценки</param>
        /// </summary>
        public Rating(int id, int studentId, int linkKind, int linkId, int value)
        {
            Id = id;
            LinkId = linkId;
            LinkKind = linkKind;
            Value = value;
            StudentId = studentId;
        }


        /// <summary>
        /// Идентификатор оценки
        /// </summary>
        public int Id { get; set; }
        /// <summary>
        /// Идентификатор студента
        /// </summary>
        public int StudentId { get; set; }
        /// <summary>
        /// Тип оценки (1 - за ДЗ, 2 - за урок)
        /// </summary>
        /// 
        public int LinkKind { get; set; }
        /// <summary>
        /// Ссылка на "Оценка за ДЗ" или "Оценка за урок"
        /// </summary>
        public int LinkId { get; set; }
       
        /// <summary>
        /// Значение оценки
        /// </summary>
        public int Value { get; set; }
        public bool IsDeleted { get; set; }

        public override string ToString()
        {
            return $"Id = {this.Id}, LinkId = {this.LinkId}, Kind = {this.LinkKind}, Value = {this.Value}";
        }
    }
}
