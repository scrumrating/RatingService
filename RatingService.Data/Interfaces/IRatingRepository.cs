﻿using RatingService.Data.Entity;
using System;
using System.Collections.Generic;
using System.Text;

namespace RatingService.Data.Interfaces
{
    public interface IRatingRepository
    {
        List<Rating> GetAll();
        Rating Get(int id);
        bool Create(Rating course);
        bool CreateOrUpdate(Rating course);
        
        bool Update(Rating course);
        bool Delete(int id);
        Rating GetLesson(int studentId, int lessonId);
        List<Rating> GetHomeWorks(int studentId, int[] homeWorks);
        int GetStudentSummary(int studentId, int[] lessonList, int[] homeWorkList);
        int GetTeamStudentsSummary(int[] studentList, int[] lessonList, int[] homeWorkList);
    }
}
