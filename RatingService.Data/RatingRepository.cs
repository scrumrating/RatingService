﻿using RatingService.Data.Entity;
using RatingService.Data.Interfaces;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace RatingService.Data
{
    public class RatingRepository:IRatingRepository
    {
        private readonly RatingDbContext context;
        private readonly int _LinkKindLesson = 1;
        private readonly int _LinkKindHomeWork = 2;

        public RatingRepository(RatingDbContext context)
        {
            this.context = context;
        }

        public bool Create(Rating rating)
        {
            context.Ratings.Add(rating);
            context.SaveChanges();
            return true;
        }

        public bool CreateOrUpdate(Rating rating)
        {
            var findRating = context.Ratings.FirstOrDefault(x => x.LinkId == rating.LinkId && x.StudentId == rating.StudentId && x.LinkKind == rating.LinkKind);
            if (findRating != null)
            {
                findRating.StudentId = rating.StudentId;
                findRating.LinkKind = rating.LinkKind;
                findRating.LinkId = rating.LinkId;
                context.Ratings.Update(findRating);
                context.SaveChanges();
                return true;
            }
            else {
                var maxId = context.Ratings.Max(x => x.Id) + 1;
                rating.Id = maxId;
                context.Ratings.Add(rating);
                context.SaveChanges();
                return true;
            }

        }

        public bool Delete(int id)
        {
            var student = context.Ratings.Find(id);
            if (student != null)
            {                
                context.Ratings.Remove(student);
                context.SaveChanges();
                return true;
            }
            return false;

        }

        public Rating Get(int id)
        {
            return context.Ratings.FirstOrDefault(x => x.Id == id && x.IsDeleted == false);
        }

        public List<Rating> GetAll()
        {
            return context.Ratings
                .Where(x => x.IsDeleted == false).ToList();
        }

        public bool Update(Rating rating)
        {
            context.Ratings.Update(rating);
            context.SaveChanges();
            return true;
        }

        public Rating GetLesson(int studentId, int lessonId)
        {
            return context.Ratings.FirstOrDefault(x => x.IsDeleted == false && x.StudentId == studentId && x.LinkKind == _LinkKindLesson && x.LinkId == lessonId);
        }
        public List<Rating> GetHomeWorks(int studentId, int[] homeWorks)
        {
            var ratings = context.Ratings.Where(x => x.IsDeleted == false && x.StudentId == studentId && x.LinkKind == _LinkKindHomeWork && homeWorks.Contains(x.LinkId)).ToList();
            foreach (var el in homeWorks)
            {
                if (!ratings.Any(x => x.LinkId == el))
                {
                    ratings.Add(new Rating(0, studentId, _LinkKindHomeWork, el, 0));
                }
            }
            return ratings;
        }

        public int GetStudentSummary(int studentId, int[] lessonList, int[] homeWorkList)
        {
            return context.Ratings.Where(x => x.IsDeleted == false 
                  && x.StudentId == studentId 
                  && ((x.LinkKind == _LinkKindHomeWork && homeWorkList.Contains(x.LinkId))
                       ||(x.LinkKind == _LinkKindLesson && lessonList.Contains(x.LinkId))
                      )
                  ).Sum(s=>s.Value);           
        }

        public int GetTeamStudentsSummary(int[] studentList, int[] lessonList, int[] homeWorkList)
        {
            return context.Ratings.Where(x => x.IsDeleted == false
                  && studentList.Contains(x.StudentId)
                  && ((x.LinkKind == _LinkKindHomeWork && homeWorkList.Contains(x.LinkId))
                       || (x.LinkKind == _LinkKindLesson && lessonList.Contains(x.LinkId))
                      )
                  ).Sum(s => s.Value);
        }
    }
}

 