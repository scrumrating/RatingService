﻿using System;
using System.Collections.Generic;
using System.Text;

namespace RatingService.Data.DTO
{
    public class SummaryTeamDto
    {
        public int TeamId { get; set; }
        public int SumRating { get; set; }
    }
}
