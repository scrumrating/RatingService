﻿using System;
using System.Collections.Generic;
using System.Text;

namespace RatingService.Data.DTO
{
    public class SummaryStudentDto
    {
        public int StudentId { get; set; }
        public int SumRating { get; set; }
    }
}
