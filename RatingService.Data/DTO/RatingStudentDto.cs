﻿using System;
using System.Collections.Generic;
using System.Text;

namespace RatingService.Data.DTO
{
    public class RatingStudentDto
    {
        public int StudentId { get; set; }
        public Lesson Lesson { get; set; }

        public RatingStudentDto(int studentId, Lesson lesson)
        {
            StudentId = studentId;
            Lesson = lesson;
        }
    }
    public class Lesson
    {
        public int Id { get; set; }
        public int Value { get; set; }
        public List<HomeWork> HomeWorks { get; set; }
    }

    public class HomeWork
    {
        public int Id { get; set; }
        public int Value { get; set; }
    }
}
