﻿using System;
using System.Collections.Generic;
using System.Text;

namespace RatingService.Data.DTO
{
    public class TeamDto
    {
        public int Id { get; set; }
        public int[] Students { get; set; }
    }
}
