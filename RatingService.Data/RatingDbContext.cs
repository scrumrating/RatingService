﻿using Microsoft.EntityFrameworkCore;
using RatingService.Data.Entity;
using System;
using System.Collections.Generic;
using System.Text;

namespace RatingService.Data
{
    /// <summary>
    /// Контекст базы данных Rating.
    /// </summary>
    public class RatingDbContext: DbContext
    {
        /// <summary>
        /// Инициализирует новый экземпляр класса <see cref="RatingDbContext"/>.
        /// </summary>
        public RatingDbContext(DbContextOptions<RatingDbContext> option) : base(option)
        {
            Database.EnsureCreated();
        }
        public DbSet<Rating> Ratings { get; set; }

        protected override void OnModelCreating(ModelBuilder builder)
        {
            /*var table = builder.Entity<Rating>();
            table.Property(x => x.Id).IsRequired().ValueGeneratedOnAdd();
            table.Property(x => x.StudentId).IsRequired();
            table.Property(x => x.LinkKind).IsRequired();
            table.Property(x => x.LinkId).IsRequired();
            */
        }
    }
}
