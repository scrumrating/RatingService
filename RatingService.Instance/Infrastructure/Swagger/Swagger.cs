﻿using Microsoft.AspNetCore.Builder;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.OpenApi.Models;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Threading.Tasks;

namespace RatingService.Instance.Infrastructure.Swagger
{
    /// <summary>
    /// Регистрация Swagger в приложении.
    /// </summary>
    internal static class Swagger
    {
        /// <summary>
        /// Регистрирует сервис документации.
        /// </summary>
        /// <param name="services">Список сервисов.</param>
        /// <param name="configuration">Конфигурация.</param>
        /// <returns>Изменённый список сервисов.</returns>
        public static IServiceCollection AddSwaggerDocumentation(this IServiceCollection services, IConfiguration configuration)
        {
            var swaggerCongiguration = configuration.GetSection("Swagger").Get<SwaggerCongiguration>();
            var apiInfo = new OpenApiInfo
            {
                Title = swaggerCongiguration.Title,
                Version = swaggerCongiguration.Version,
                Description = swaggerCongiguration.Description,
            };
            return services.AddSwaggerGen(c =>
            {
                c.SwaggerDoc(swaggerCongiguration.Version, apiInfo);
                var xmlFile = swaggerCongiguration.XmlFile;
                var xmlPath = Path.Combine(AppContext.BaseDirectory, xmlFile);
                c.IncludeXmlComments(xmlPath);
            });
        }

        /// <summary>
        /// Добавляет middleware сервиса документации.
        /// </summary>
        /// <param name="application">Application builder.</param>
        /// <param name="configuration">Application configuration.</param>
        /// <returns>Configured application builder.</returns>
        public static IApplicationBuilder UseSwaggerDocumentation(this IApplicationBuilder application, IConfiguration configuration)
        {
            var swaggerCongiguration = configuration.GetSection("Swagger").Get<SwaggerCongiguration>();
            application.UseSwagger();
            application.UseSwaggerUI(c =>
            {
                c.SwaggerEndpoint(swaggerCongiguration.Uri, swaggerCongiguration.Name);
            });
            return application;
        }
    }
}