﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace RatingService.Instance.Infrastructure.Swagger
{
    /// <summary>
    /// Конфигурация swagger.
    /// </summary>
    public class SwaggerCongiguration
    {
        /// <summary>
        /// Путь к UI swagger
        /// </summary>
        public string Uri { get; set; }

        /// <summary>
        /// Название
        /// </summary>
        public string Name { get; set; }

        /// <summary>
        /// Заголовок
        /// </summary>
        public string Title { get; set; }

        /// <summary>
        /// Версия
        /// </summary>
        public string Version { get; set; }

        /// <summary>
        /// Описание странички.
        /// </summary>
        public string Description { get; set; }

        /// <summary>
        /// Результирующий файл XML
        /// </summary>
        public string XmlFile { get; set; }        
    }
}