﻿using MassTransit;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Hosting;
using RatingService.Consumers.Create;
using RatingService.Consumers.CreateOrUpdate;
using RatingService.Consumers.Delete;
using RatingService.Consumers.Get;
using RatingService.Consumers.GetAll;
using RatingService.Consumers.GetRatingStudentList;
using RatingService.Consumers.GetSummaryStudentList;
using RatingService.Consumers.GetSummaryTeamList;
using RatingService.Consumers.Infrastructure;
using RatingService.Consumers.Update;
using System;

namespace RatingService.Instance.Infrastructure.MessageBroker
{
    internal static class MessageBroker
    {
        /// <summary>
        /// Регистрирует броекр сообщений.
        /// </summary>
        /// <param name="services">Список сервисов.</param>
        /// <returns>Изменённый список сервисов.</returns>
        public static IServiceCollection AddMessageBroker(this IServiceCollection services, IConfiguration configuration)
        {
            services.AddConsumers();

            // Регистрация потребителей сообщений
            services.AddMassTransit(x =>
            {
                x.AddConsumer<GetAllConsumer>();
                x.AddConsumer<GetOneConsumer>();
                x.AddConsumer<UpdateConsumer>();
                x.AddConsumer<CreateConsumer>();
                x.AddConsumer<DeleteConsumer>();
                x.AddConsumer<GetRatingStudentListConsumer>();
                x.AddConsumer<GetSummaryStudentListConsumer>();
                x.AddConsumer<GetSummaryTeamListConsumer>();
                x.AddConsumer<CreateOrUpdateConsumer>();
            });


            #region === Bus ===
            // Регистрация шины.
            // Подробнее про регистрацию шины можно почитать здесь: https://masstransit-project.com/usage/
            services.AddSingleton(serviceProvider => Bus.Factory.CreateUsingRabbitMq(configure =>
            {
                BusConfiguration busConfiguration = configuration.GetSection("Bus").Get<BusConfiguration>();

                // Конфигурация подключения к шине, включающая в себя указание адреса и учетных данных.
                configure.Host(new Uri(busConfiguration.ConnectionString), host =>
                {
                    host.Username(busConfiguration.Username);
                    host.Password(busConfiguration.Password);

                    // Подтверждение получения гарантирует доставку сообщений за счет ухудшения производительности.
                    host.PublisherConfirmation = busConfiguration.PublisherConfirmation;
                });

                // Добавление Serilog для журналирования внутренностей MassTransit.
                configure.UseSerilog();

                // Регистрация очередей и их связи с потребителями сообщений.
                // В качестве метки сообщения используется полное имя класса сообщения, которое потребляет потребитель.


                configure.ReceiveEndpoint(typeof(GetAllCommand).FullName, endpoint => { endpoint.Consumer<GetAllConsumer>(serviceProvider); EndpointConvention.Map<GetAllCommand>(endpoint.InputAddress); });
                configure.ReceiveEndpoint(typeof(GetCommand).FullName, endpoint => { endpoint.Consumer<GetOneConsumer>(serviceProvider); EndpointConvention.Map<GetCommand>(endpoint.InputAddress); });
                configure.ReceiveEndpoint(typeof(CreateCommand).FullName, endpoint => { endpoint.Consumer<CreateConsumer>(serviceProvider); EndpointConvention.Map<CreateCommand>(endpoint.InputAddress); });
                configure.ReceiveEndpoint(typeof(CreateOrUpdateCommand).FullName, endpoint => { endpoint.Consumer<CreateOrUpdateConsumer>(serviceProvider); EndpointConvention.Map<CreateOrUpdateCommand>(endpoint.InputAddress); });
                configure.ReceiveEndpoint(typeof(UpdateCommand).FullName, endpoint => { endpoint.Consumer<UpdateConsumer>(serviceProvider); EndpointConvention.Map<UpdateCommand>(endpoint.InputAddress); });
                configure.ReceiveEndpoint(typeof(DeleteCommand).FullName, endpoint => { endpoint.Consumer<DeleteConsumer>(serviceProvider); EndpointConvention.Map<DeleteCommand>(endpoint.InputAddress); });
                configure.ReceiveEndpoint(typeof(GetRatingStudentListCommand).FullName, endpoint => { endpoint.Consumer<GetRatingStudentListConsumer>(serviceProvider); EndpointConvention.Map<GetRatingStudentListCommand>(endpoint.InputAddress); });
                configure.ReceiveEndpoint(typeof(GetSummaryStudentListCommand).FullName, endpoint => { endpoint.Consumer<GetSummaryStudentListConsumer>(serviceProvider); EndpointConvention.Map<GetSummaryStudentListCommand>(endpoint.InputAddress); });
                configure.ReceiveEndpoint(typeof(GetSummaryTeamListCommand).FullName, endpoint => { endpoint.Consumer<GetSummaryTeamListConsumer>(serviceProvider); EndpointConvention.Map<GetSummaryTeamListCommand>(endpoint.InputAddress); });

            }));

            // Регистрация сервисов MassTransit.
            services.AddSingleton<IPublishEndpoint>(serviceProvider => serviceProvider.GetRequiredService<IBusControl>());
            services.AddSingleton<ISendEndpointProvider>(serviceProvider => serviceProvider.GetRequiredService<IBusControl>());
            services.AddSingleton<IBus>(serviceProvider => serviceProvider.GetRequiredService<IBusControl>());

            #endregion

            services.AddScoped(serviceProvider => serviceProvider.GetRequiredService<IBus>().CreateRequestClient<GetAllCommand>());
            services.AddScoped(serviceProvider => serviceProvider.GetRequiredService<IBus>().CreateRequestClient<GetCommand>());
            services.AddScoped(serviceProvider => serviceProvider.GetRequiredService<IBus>().CreateRequestClient<CreateCommand>());
            services.AddScoped(serviceProvider => serviceProvider.GetRequiredService<IBus>().CreateRequestClient<CreateOrUpdateCommand>());
            services.AddScoped(serviceProvider => serviceProvider.GetRequiredService<IBus>().CreateRequestClient<UpdateCommand>());
            services.AddScoped(serviceProvider => serviceProvider.GetRequiredService<IBus>().CreateRequestClient<DeleteCommand>());
            services.AddScoped(serviceProvider => serviceProvider.GetRequiredService<IBus>().CreateRequestClient<GetRatingStudentListCommand>());
            services.AddScoped(serviceProvider => serviceProvider.GetRequiredService<IBus>().CreateRequestClient<GetSummaryStudentListCommand>());
            services.AddScoped(serviceProvider => serviceProvider.GetRequiredService<IBus>().CreateRequestClient<GetSummaryTeamListCommand>());



            // Регистрация сервиса шины MassTransit.
            services.AddSingleton<IHostedService, BusService>();
            return services;
        }
    }
}
