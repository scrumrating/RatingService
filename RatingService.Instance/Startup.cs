using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Threading.Tasks;
using MassTransit;
using Microsoft.AspNetCore.Builder;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.HttpsPolicy;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Hosting;
using Microsoft.Extensions.Logging;
using Microsoft.OpenApi.Models;
using RatingService.BI.ServiceInterfaces;
using RatingService.Consumers.Create;
using RatingService.Consumers.Delete;
using RatingService.Consumers.Get;
using RatingService.Consumers.GetAll;
using RatingService.Consumers.GetRatingStudentList;
using RatingService.Consumers.Update;
using RatingService.Data;
using RatingService.Data.Interfaces;
using RatingService.Instance.Infrastructure.MessageBroker;
using RatingService.Instance.Infrastructure.Swagger;
using Serilog;

namespace RatingService.Instance
{
    /// <summary>
    /// ������������� ����������.
    /// </summary>
    public class Startup
    {
        private readonly IConfiguration configuration;
        /// <summary>
        /// �������������� ����� ��������� ������ <see cref="Startup"/>.
        /// </summary>

        public Startup(IConfiguration configuration)
        {
            this.configuration = configuration;
            ConfigureDataStorage();
        }

        /// <summary>
        /// ������������ ����������.
        /// </summary>
        /// <param name="application">����������.</param>
        public void Configure(IApplicationBuilder app)
        {
            app.UseCors("AllowAll");
            app.UseMvc();           
            app.UseSwaggerDocumentation(this.configuration);

        }

        /// <summary>
        /// ������������ ��������.
        /// </summary>
        /// <param name="services">�������.</param>

        public void ConfigureServices(IServiceCollection services)
        {
            Log.Logger = new LoggerConfiguration()
                    .ReadFrom.Configuration(this.configuration)
                    .CreateLogger();

            services.AddCors(o => o.AddPolicy("AllowAll", builder =>
            {
                builder.AllowAnyOrigin()
                    .AllowAnyMethod()
                    .AllowAnyHeader();
            }));



            Log.Information("����������� ������� Rating");
            //Swager
            services.AddSwaggerDocumentation(this.configuration);

            //DB
            services.AddScoped<DbContext, RatingDbContext>();
            services.AddDbContext<RatingDbContext>(option =>
                option.UseNpgsql(this.configuration.GetConnectionString("Rating")));

            //services
            services.AddScoped<IRatingService, BI.Services.RatingService>();
            services.AddScoped<IRatingRepository, RatingRepository>();
            services.AddMessageBroker(this.configuration);
            Log.Information("������ Rating ���������������.");            

            Log.Information("���������� ����������� �������� MVC.");
            services.AddMvc();
            Log.Information("����������� �������� MVC ������� ���������.");
        }
        /// <summary>
        /// ������������� ���� ������.
        /// </summary>
        public void ConfigureDataStorage()
        {
            Log.Information("���������� ��������/���������� ���� ������.");

            var contextOptions = new DbContextOptionsBuilder<RatingDbContext>();
            contextOptions.UseNpgsql(this.configuration.GetConnectionString("Rating"));

            using (RatingDbContext context = new RatingDbContext(contextOptions.Options))
            {
                context.Database.EnsureCreated();
            }

            Log.Information("��������/���������� ���� ������ ���������.");
        }

    }
}
