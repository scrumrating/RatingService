using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.IO;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.Hosting.WindowsServices;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.Hosting;
using Microsoft.Extensions.Logging;
using Serilog;

namespace RatingService.Instance
{
    class Program
    {
        /// <summary>
        /// ����� ����� � ����������.
        /// </summary>
        /// <param name="args">��������� ������� ����������.</param>
        public static void Main(string[] args)
        {
            Log.Logger = new LoggerConfiguration()
                .WriteTo.Console()
                .WriteTo.File(@"logs\\startup-.log", rollingInterval: RollingInterval.Day)
                .CreateLogger();

            try
            {
                Log.Information("���������� ������ ����������.");
                if (args.Contains("--service") || args.Contains("-s"))
                {
                    Log.Information("������� ����� '--service' ��� '-s'. ���������� ����� �������� ��� windows-������.");
                    string contentRootPath = Path.GetDirectoryName(Process.GetCurrentProcess().MainModule?.FileName);

                    Log.Information("���������� ���������� ����� ����������.");

                    IWebHost host = WebHost.CreateDefaultBuilder(args)
                        .UseContentRoot(contentRootPath)
                        .UseStartup<Startup>()
                        .UseSerilog()
                        .Build();

                    Log.Information("���������� ����� ���������� ������� ���������.");

                    host.RunAsService();
                }
                else
                {
                    Log.Information("���������� ����� �������� ��� ���������� ����������. ��� ����, ��� �� ��������� ���������� ��� windows-������, ��������� ����� '--service' ��� '-s'.");

                    Log.Information("���������� ���������� ����� ����������.");

                    IWebHost host = WebHost.CreateDefaultBuilder(args)
                        .UseContentRoot(Directory.GetCurrentDirectory())
                        .UseStartup<Startup>()
                        .UseSerilog()
                        .Build();

                    Log.Information("���������� ����� ���������� ������� ���������.");

                    host.Run();
                }
            }
            catch (Exception ex)
            {
                Log.Fatal(ex, "���������� ���������� ���������.");
            }
            finally
            {
                Log.Information("���������� �����������.");
                Log.CloseAndFlush();
            }
        }
    }
}
