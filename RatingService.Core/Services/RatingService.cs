﻿using RatingService.Core.DTO;
using RatingService.Core.Entity;
using RatingService.Core.Interfaces;
using System;
using System.Collections.Generic;
using System.Text;
using Microsoft.Extensions.Logging;
using System.Linq;

namespace RatingService.Core.Services
{
    public class RatingService : IGenericService<Rating>
    {
        private readonly IGenericRepository<RatingDto> repository;
        private readonly ILogger<RatingService> logger;

        /// <summary>
        /// Инициализирует новый экземпляр класса <see cref="RatingService"/>.
        /// </summary>
        /// <param name="repository">Хранилище данных</param>
        /// <param name="logger">Абстракция над сервисом журналирования.</param>
        public RatingService(IGenericRepository<RatingDto> repository, ILogger<RatingService> logger)
        {
            this.repository = repository;
            this.logger = logger;
        }
        /// <inheritdoc />
        public bool Create(Rating item)
        {
            this.logger.LogInformation($"Выполняется создание оценки {item}.");
            this.repository.Create(new RatingDto()
            {
                LinkId = item.LinkId,
                Kind = item.Kind,
                Value = item.Value
            });
            return true;
        }


        /// <inheritdoc />
        public Rating FindById(int id)
        {
            this.logger.LogInformation($"поиск оценки по id = {id}");
            var rating = this.repository.FindById(id);

            if (rating != null)
                return new Rating(rating.Id, rating.LinkId, rating.Kind, rating.Value);

            return null;
        }
        /// <inheritdoc />
        public IEnumerable<Rating> Get()
        {
            this.logger.LogInformation($"Поиск всех оценок");
            return 
                this.repository.Get()
                .Select(x => new Rating(x.Id, x.LinkId, x.Kind, x.Value));

        }
        /// <inheritdoc />
        public IEnumerable<Rating> Get(Func<Rating, bool> predicate)
        {
            this.logger.LogInformation($"Поиск всех оценок c eckjdbtv {predicate}");
            return
                this.repository.Get()
                .Select(x => new Rating(x.Id, x.LinkId, x.Kind, x.Value))
                .Where(predicate);

        }

        public void Remove(Rating item)
        {
            this.logger.LogInformation($"Выполняется удаление оценки {item}.");
        }

        /// <inheritdoc />
        public void Update(Rating item)
        {
            this.logger.LogInformation($"Выполняется обновление данных оценки {item}.");
            this.repository.Update(new RatingDto()
            {
                Id = item.Id,
                LinkId = item.LinkId,
                Kind = item.Kind,
                Value = item.Value
            });
        }
    }
}
