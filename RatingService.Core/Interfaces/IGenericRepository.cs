﻿using RatingService.Core.DTO;
using System;
using System.Collections.Generic;
using System.Text;

namespace RatingService.Core.Interfaces
{
    /// <summary>
    /// Интерфейс репозитория, чтобы был общий у всех.
    /// </summary>
    /// <typeparam name="TEntityDTO"></typeparam>
    public interface IGenericRepository<TEntityDTO> where TEntityDTO : IdentityDto
    {
        /// <summary>
        /// Создание записи.
        /// </summary>
        /// <param name="item">Сущность для создания.</param>
        /// <returns>Успешное добавление.</returns>
        bool Create(TEntityDTO item);

        /// <summary>
        /// Найти и вернуть сущность по ID.
        /// </summary>
        /// <param name="id">Чиcловой идентификатор записи.</param>
        /// <returns>Найденная сущность или null.</returns>
        TEntityDTO FindById(int id);

        /// <summary>
        /// Получить коллекцию строк.
        /// </summary>
        /// <returns>Выборка данных.</returns>
        IEnumerable<TEntityDTO> Get();

        /// <summary>
        /// Получить коллекцию строк по заданному фильтру.
        /// </summary>
        /// <param name="predicate">Условие по кторому будут браться даныне.</param>
        /// <returns>Выборка данных.</returns>
        IEnumerable<TEntityDTO> Get(Func<TEntityDTO, bool> predicate);

        /// <summary>
        /// Удалить запись.
        /// </summary>
        /// <param name="item">Сущность для удаления.</param>
        void Remove(TEntityDTO item);

        /// <summary>
        /// Измменить запись.
        /// </summary>
        /// <param name="item">Обновление записи.</param>
        void Update(TEntityDTO item);
    }
}
