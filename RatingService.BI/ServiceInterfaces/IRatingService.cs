﻿using RatingService.Data.DTO;
using System;
using System.Collections.Generic;
using System.Text;

namespace RatingService.BI.ServiceInterfaces
{
    public interface IRatingService
    {
        List<RatingDto> GetAll();
        RatingDto Get(int id);
        bool Create(RatingDto rating);
        bool CreateOrUpdate(RatingDto rating);
        bool Update(RatingDto rating);
        bool Delete(int id);
        List<RatingStudentDto> GetRatingStudentList(int[] studentList, int lessonId, int[] homeWorkList);
        List<SummaryStudentDto> GetSummaryStudentList(int[] studentList, int[] lessonList, int[] homeWorkList);
        List<SummaryTeamDto> GetSummaryTeamList(TeamDto[] teamList, int[] lessonList, int[] homeWorkList);
        
    }
}
