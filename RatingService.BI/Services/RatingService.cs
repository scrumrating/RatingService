﻿using RatingService.BI.ServiceInterfaces;
using RatingService.Data;
using RatingService.Data.DTO;
using RatingService.Data.Entity;
using RatingService.Data.Interfaces;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace RatingService.BI.Services
{
    public class RatingService : IRatingService
    {
        private readonly IRatingRepository repo;

        public RatingService(IRatingRepository repo)
        {
            this.repo = repo;
        }

        public bool Create(RatingDto rating)
        {
            return this.repo.Create(new Rating(rating.Id,rating.StudentId, rating.LinkKind , rating.LinkId, rating.Value));
        }

        public bool Delete(int id)
        {
            return repo.Delete(id);
        }

        public RatingDto Get(int id)
        {
            var item = repo.Get(id);
            if (item != null)
            {
                return new RatingDto(item.Id,item.StudentId, item.LinkKind, item.LinkId, item.Value);
            }
            return null;
        }

        public List<RatingDto> GetAll()
        {
            var list = repo.GetAll();
            return list.Select(x => new RatingDto(x.Id,x.StudentId, x.LinkKind, x.LinkId, x.Value)).ToList();
        }

        public bool Update(RatingDto rating)
        {
            return repo.Update(new Rating(rating.Id, rating.StudentId,rating.LinkKind, rating.LinkId, rating.Value));
        }

        public List<RatingStudentDto> GetRatingStudentList(int[] strudentList, int LessonId, int[] homeWorkList)
        {
            var list = new List<RatingStudentDto>();
            foreach (var studentId in strudentList)
            {
                var lesson = new Lesson();
                var lessonDto = repo.GetLesson(studentId, LessonId);
                if (lessonDto != null)
                {
                    var homeWorkListDto = repo.GetHomeWorks(studentId, homeWorkList);
                    lesson = new Lesson()
                    {
                        Id = lessonDto.LinkId,
                        Value = lessonDto.Value,
                        HomeWorks = repo.GetHomeWorks(studentId, homeWorkList).Select(x => new HomeWork() { Id = x.LinkId, Value = x.Value }).ToList()
                    };

                }
                list.Add(new RatingStudentDto(studentId, lesson));

            }
            return list;
        }
        public List<SummaryStudentDto> GetSummaryStudentList(int[] studentList, int[] lessonList, int[] homeWorkList)
        {
            var list = new List<SummaryStudentDto>();
            foreach (var studentId in studentList)
            {
                list.Add(new SummaryStudentDto() { StudentId = studentId, SumRating = repo.GetStudentSummary(studentId, lessonList, homeWorkList) });                
            }
            return list;
        }

        public List<SummaryTeamDto> GetSummaryTeamList(TeamDto[] teamList, int[] lessonList, int[] homeWorkList)
        {
            var list = new List<SummaryTeamDto>();
            foreach (var team in teamList)
            {               
                list.Add(new SummaryTeamDto() { TeamId = team.Id, SumRating = repo.GetTeamStudentsSummary(team.Students, lessonList, homeWorkList) }); 
            }
            return list;
        }

        public bool CreateOrUpdate(RatingDto rating)
        {
            return this.repo.CreateOrUpdate(new Rating(rating.Id, rating.StudentId, rating.LinkKind, rating.LinkId, rating.Value));
        }
    }
}
