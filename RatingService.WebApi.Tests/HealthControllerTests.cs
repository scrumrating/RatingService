﻿using FakeItEasy;
using FluentAssertions;
using MassTransit;
using Microsoft.AspNetCore.Mvc;
using NUnit.Framework;
using RatingService.Consumers.Healthchecks;
using RatingService.WebApi.Controllers;
using System;
using System.Collections.Generic;
using System.Net;
using System.Text;
using System.Threading;

namespace RatingService.WebApi.Tests
{
    public class HealthControllerTests
    {
        [Test]
        [Description("Запрос статуса службы должен завершаться успешно.")]
        public void CanCheckHealth()
        {
            Response<HealthcheckResponse> response = A.Fake<Response<HealthcheckResponse>>();
            A.CallTo(() => response.Message).Returns(new HealthcheckResponse { Result = "success" });
            IRequestClient<HealthcheckCommand> client = A.Fake<IRequestClient<HealthcheckCommand>>();
            A.CallTo(() => client.GetResponse<HealthcheckResponse>(
                    A<HealthcheckCommand>._,
                    A<CancellationToken>._,
                    A<RequestTimeout>._))
                .Returns(response);

            var sut = new HealthController(client);
            var result = sut.Get().GetAwaiter().GetResult() as StatusCodeResult;

            result.Should().NotBe(null);
            result.StatusCode.Should().Be((int)HttpStatusCode.OK);
        }
    }
}