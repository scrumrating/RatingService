﻿using System;
using System.Collections.Generic;
using System.Text;

namespace RatingService.WebApi.ViewModel
{
    public class GetRatingStudentListViewModel
    {
        public int[] StrudentList { get; set; }
        public int LessonId { get; set; }
        public int[] HomeWorkList { get; set; }        
    }
}
