﻿using RatingService.Data.DTO;
using System;
using System.Collections.Generic;
using System.Text;

namespace RatingService.WebApi.ViewModel
{
     public class GetSummaryTeamListViewModel
    {
        public TeamDto[] TeamList { get; set; }
        public int[] LessonList { get; set; }
        public int[] HomeWorkList { get; set; }
    }    
}
