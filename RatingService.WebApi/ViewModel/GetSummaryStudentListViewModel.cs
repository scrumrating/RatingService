﻿using System;
using System.Collections.Generic;
using System.Text;

namespace RatingService.WebApi.ViewModel
{
   public class GetSummaryStudentListViewModel
    {
        public int[] StrudentList { get; set; }
        public int[] LessonList { get; set; }
        public int[] HomeWorkList { get; set; }
    }
}
