﻿using System;
using System.Collections.Generic;
using System.Net;
using System.Text;
using System.Threading.Tasks;
using MassTransit;
using Microsoft.AspNetCore.Mvc;
using RatingService.Consumers.Create;
using RatingService.Consumers.CreateOrUpdate;
using RatingService.Consumers.Delete;
using RatingService.Consumers.Get;
using RatingService.Consumers.GetAll;
using RatingService.Consumers.GetRatingStudentList;
using RatingService.Consumers.GetSummaryStudentList;
using RatingService.Consumers.GetSummaryTeamList;
using RatingService.Consumers.Update;
using RatingService.Core.Structures;
using RatingService.Data.DTO;
using RatingService.WebApi.ViewModel;

namespace RatingService.WebApi.Controllers
{
    /// <summary>
    /// Контроллер Rating.
    /// </summary>
    [Route("api/rating")]
    [ApiController]
    public class RatingController : ControllerBase
    {
        private readonly IRequestClient<GetAllCommand> getAllClient;
        private readonly IRequestClient<GetCommand>    getClient;
        private readonly IRequestClient<CreateCommand> createClient;
        private readonly IRequestClient<CreateOrUpdateCommand> createOrUpdateClient;
        private readonly IRequestClient<UpdateCommand> updateClient;
        private readonly IRequestClient<DeleteCommand> deleteClient;
        private readonly IRequestClient<GetRatingStudentListCommand> getRatingStudentListClient;
        private readonly IRequestClient<GetSummaryStudentListCommand> getSummaryStudentListClient;
        private readonly IRequestClient<GetSummaryTeamListCommand> getSummaryTeamListClient;
        


        /// <summary>
        /// Инициализирует новый экземпляр класса <see cref="StudentsController"/>.
        /// </summary>
        /// <param name="getAllClient">Клиент получает все записи</param>
        /// <param name="getClient">Клиент одну запись по id</param>
        /// <param name="createClient">Клиент создает 1 запись.</param>
        /// <param name="updateClient">Клиент обновляет 1 запись</param>
        /// <param name="deleteClient">Удаляет одну запись по id</param>
        /// <param name="getRatingStudentListClient">Клент получает список студентов с детализацией уроков и ДЗ</param>

        public RatingController(IRequestClient<GetAllCommand> getAllClient,
                                IRequestClient<GetCommand> getClient,
                                IRequestClient<CreateCommand> createClient,
                                IRequestClient<CreateOrUpdateCommand> createOrUpdateClient,                                
                                IRequestClient<UpdateCommand> updateClient,
                                IRequestClient<DeleteCommand> deleteClient,
                                IRequestClient<GetRatingStudentListCommand> getRatingStudentListClient,
                                IRequestClient<GetSummaryStudentListCommand> getSummaryStudentListClient,
                                IRequestClient<GetSummaryTeamListCommand> getSummaryTeamListClient)
        {
            this.getAllClient = getAllClient;
            this.getClient = getClient;
            this.createClient = createClient;
            this.createOrUpdateClient = createOrUpdateClient;            
            this.updateClient = updateClient;
            this.deleteClient = deleteClient;
            this.getRatingStudentListClient = getRatingStudentListClient;
            this.getSummaryStudentListClient = getSummaryStudentListClient;
            this.getSummaryTeamListClient = getSummaryTeamListClient;
        }

        /// <summary>
        /// Получить информацию о всех оценках
        /// </summary>
        /// <returns>Список всех оценок</returns>
        [HttpGet]
        public async Task<IActionResult> Get()
        {
            try
            {
                var command = new GetAllCommand();
                var response = await this.getAllClient.GetResponse<GetAllResponse>(command);

                switch (response.Message.Result)
                {
                    case "success":
                        return this.Ok(response.Message.Ratings);
                    case "not-found":
                        return new StatusCodeResult((int)HttpStatusCode.NotFound);
                    default:
                        return new StatusCodeResult((int)HttpStatusCode.BadGateway);
                }
            }
            catch (RequestTimeoutException)
            {
                return new StatusCodeResult((int)HttpStatusCode.GatewayTimeout);
            }
            catch (Exception)
            {
                return new StatusCodeResult((int)HttpStatusCode.BadGateway);
            }
        }
        /// <summary>
        /// Получить инфо об оценке по id
        /// </summary>
        /// <param name="id">Идентификатор оценки</param>
        /// <returns>Курс</returns>
        [HttpGet]
        [Route("GetById")]
        public async Task<IActionResult> GetById([FromQuery(Name = "id")] int id)
        {
            try
            {
                var response = await this.getClient.GetResponse<GetResponse>(new GetCommand { Id = id });

                switch (response.Message.Result)
                {
                    case "success":
                        return this.Ok(response.Message.Rating);
                    case "not-found":
                        return new StatusCodeResult((int)HttpStatusCode.NotFound);
                    default:
                        return new StatusCodeResult((int)HttpStatusCode.BadGateway);
                }
            }
            catch (RequestTimeoutException)
            {
                return new StatusCodeResult((int)HttpStatusCode.GatewayTimeout);
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex.Message);
                return new StatusCodeResult((int)HttpStatusCode.BadGateway);
            }
        }

        /// <summary>
        /// Получить список студентов с детализацией уроков и ДЗ
        /// </summary>
        /// <param name="model">Модель для запроса GetRatingStudentListViewModel</param>
        /// <returns>Список</returns>
        [HttpGet]
        [Route("GetRatingStudentList")]
        
         public async Task<IActionResult> GetRatingStudentList([FromBody] GetRatingStudentListViewModel model)
        {
            try
            {
                var response = await this.getRatingStudentListClient.GetResponse<GetRatingStudentListResponse>(
                    new GetRatingStudentListCommand ( model.StrudentList, model.LessonId, model.HomeWorkList));

                switch (response.Message.Result)
                {
                    case "success":
                        return this.Ok(response.Message.RatingStudent);
                    case "not-found":
                        return new StatusCodeResult((int)HttpStatusCode.NotFound);
                    default:
                        return new StatusCodeResult((int)HttpStatusCode.BadGateway);
                }
            }
            catch (RequestTimeoutException)
            {
                return new StatusCodeResult((int)HttpStatusCode.GatewayTimeout);
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex.Message);
                return new StatusCodeResult((int)HttpStatusCode.BadGateway);
            }
        }

        /// <summary>
        /// Получить список студентов суммой оценок уроков и ДЗ
        /// </summary>
        /// <param name="model">Модель для запроса GetRatingStudentListViewModel</param>
        /// <returns>Список</returns>
        [HttpPost]
        [Route("GetSummaryStudentList")]

        public async Task<IActionResult> GetSummaryStudentList([FromBody] GetSummaryStudentListViewModel model)
        {
            try
            {
                var response = await this.getSummaryStudentListClient.GetResponse<GetSummaryStudentListResponse>(
                    new GetSummaryStudentListCommand(model.StrudentList, model.LessonList, model.HomeWorkList));

                switch (response.Message.Result)
                {
                    case "success":
                        return this.Ok(response.Message.SummaryStudent);
                    case "not-found":
                        return new StatusCodeResult((int)HttpStatusCode.NotFound);
                    default:
                        return new StatusCodeResult((int)HttpStatusCode.BadGateway);
                }
            }
            catch (RequestTimeoutException)
            {
                return new StatusCodeResult((int)HttpStatusCode.GatewayTimeout);
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex.Message);
                return new StatusCodeResult((int)HttpStatusCode.BadGateway);
            }
        }

        /// <summary>
        /// Получить список команд суммой оценок уроков и ДЗ
        /// </summary>
        /// <param name="model">Модель для запроса GetRatingStudentListViewModel</param>
        /// <returns>Список</returns>
        [HttpPost]
        [Route("GetSummaryTeamList")]

        public async Task<IActionResult> GetSummaryTeamList([FromBody] GetSummaryTeamListViewModel model)
        {

            try
            {
                var response = await this.getSummaryTeamListClient.GetResponse<GetSummaryTeamListResponse>(
                    new GetSummaryTeamListCommand(model.TeamList, model.LessonList, model.HomeWorkList));

                switch (response.Message.Result)
                {
                    case "success":
                        return this.Ok(response.Message.SummaryTeam);
                    case "not-found":
                        return new StatusCodeResult((int)HttpStatusCode.NotFound);
                    default:
                        return new StatusCodeResult((int)HttpStatusCode.BadGateway);
                }
            }
            catch (RequestTimeoutException)
            {
                return new StatusCodeResult((int)HttpStatusCode.GatewayTimeout);
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex.Message);
                return new StatusCodeResult((int)HttpStatusCode.BadGateway);
            }
        }

        /// <summary>
        /// Создать оценку
        /// </summary>
        /// <param name="rating">Данные о оценке</param>
        [HttpPost]
        public async Task<IActionResult> CreateOne([FromBody] RatingDto rating)
        {
            try
            {
                Response<CreateResponse> response = await this.createClient.GetResponse<CreateResponse>(new CreateCommand { Rating = rating });

                switch (response.Message.Result)
                {
                    case "success":
                        return Ok();
                    case "not-created":
                        return new StatusCodeResult((int)HttpStatusCode.InternalServerError);
                    default:
                        return new StatusCodeResult((int)HttpStatusCode.BadGateway);
                }
            }
            catch (RequestTimeoutException)
            {
                return new StatusCodeResult((int)HttpStatusCode.GatewayTimeout);
            }
            catch (Exception)
            {
                return new StatusCodeResult((int)HttpStatusCode.BadGateway);
            }
        }

        /// <summary>
        /// Создать или изменить оценку
        /// </summary>
        /// <param name="rating">Данные о оценке</param>
        [HttpPost]
        [Route("AddOrUpdate")]
        public async Task<IActionResult> AddOrUpdate([FromBody] RatingDto rating)
        {
            try
            {
                var response = await this.createOrUpdateClient.GetResponse<CreateOrUpdateResponse>(new CreateOrUpdateCommand { Rating = rating });

                switch (response.Message.Result)
                {
                    case "success":
                        return Ok();
                    case "not-created":
                        return new StatusCodeResult((int)HttpStatusCode.InternalServerError);
                    default:
                        return new StatusCodeResult((int)HttpStatusCode.BadGateway);
                }
            }
            catch (RequestTimeoutException)
            {
                return new StatusCodeResult((int)HttpStatusCode.GatewayTimeout);
            }
            catch (Exception ex)
            {
                return new StatusCodeResult((int)HttpStatusCode.BadGateway);
            }
        }

        /// <summary>
        /// Обновить оценку
        /// </summary>
        /// <param name="rating">Данные о оценке</param>
        [HttpPatch]
        public async Task<IActionResult> UpdateOne([FromBody] RatingDto rating)
        {
            try
            {
                var response = await this.updateClient.GetResponse<UpdateResponse>(new UpdateCommand { Rating = rating });

                switch (response.Message.Result)
                {
                    case "success":
                        return Ok();
                    default:
                        return new StatusCodeResult((int)HttpStatusCode.BadGateway);
                }
            }
            catch (RequestTimeoutException)
            {
                return new StatusCodeResult((int)HttpStatusCode.GatewayTimeout);
            }
            catch (Exception)
            {
                return new StatusCodeResult((int)HttpStatusCode.BadGateway);
            }
        }

        /// <summary>
        /// Удалить оценку
        /// </summary>
        /// <param name="id">Идентификатор</param>
        [HttpDelete]
        public async Task<IActionResult> DeleteOne([FromQuery] int id)
        {
            try
            {
                var response = await this.deleteClient.GetResponse<DeleteResponse>(new DeleteCommand { Id = id });

                switch (response.Message.Result)
                {
                    case "success":
                        return Ok();
                    default:
                        return new StatusCodeResult((int)HttpStatusCode.BadGateway);
                }
            }
            catch (RequestTimeoutException)
            {
                return new StatusCodeResult((int)HttpStatusCode.GatewayTimeout);
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex.Message);
                return new StatusCodeResult((int)HttpStatusCode.BadGateway);
            }
        }

        

    }
}