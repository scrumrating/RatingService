﻿using RatingService.Data.DTO;
using System.Collections.Generic;

namespace RatingService.Consumers.GetAll
{
    /// <summary>
    /// Ответ на команду получения списка оценок
    /// </summary>
    public class GetAllResponse
    {
        /// <summary>
        /// Список оценок
        /// </summary>
        public List<RatingDto> Ratings { get; set; }

        /// <summary>
        /// Результат обработки команды
        /// </summary>
        public string Result { get; set; }

        public GetAllResponse(List<RatingDto> ratings, string result)
        {
            Ratings = ratings;
            Result = result;
        }

        public GetAllResponse(string result)
        {
            Result = result;
        }
        public GetAllResponse()
        {

        }
    }
}
