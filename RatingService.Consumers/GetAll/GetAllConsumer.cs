﻿using MassTransit;
using RatingService.BI.ServiceInterfaces;
using Serilog;
using System.Linq;
using System.Threading.Tasks;

namespace RatingService.Consumers.GetAll
{
    /// <summary>
    /// Обработчик получения списка оценок
    /// </summary>
    public class GetAllConsumer : IConsumer<GetAllCommand>
    {
        private readonly IRatingService service;

        /// <summary>
        /// Инициализирует новый экземпляр класса <see cref="GetOneConsumer"/>.
        /// </summary>
        /// <param name="service">Сервисный объект.</param>
        /// <param name="logger">Абстракция над сервисом журналирования.</param>
        public GetAllConsumer(IRatingService service)
        {
            this.service = service;
        }

        /// <summary>
        /// Обработать сообщение.
        /// </summary>
        /// <param name="context">Контекст обработки сообщения.</param>
        /// <returns>Асинхронная операция <see cref="Task"/>.</returns>
        public async Task Consume(ConsumeContext<GetAllCommand> context)
        {
            var list = this.service.GetAll();

            if (list != null)
                await context.RespondAsync(new GetAllResponse (list, ConsumerConsts.success ));
            else
                await context.RespondAsync(new GetAllResponse(ConsumerConsts.notfound));
        }
    }
}
