﻿using RatingService.Data.DTO;
using System;
using System.Collections.Generic;
using System.Text;

namespace RatingService.Consumers.Update
{
    public class UpdateResponse
    {
        public RatingDto Course { get; set; }
        public string Result { get; set; }

        public UpdateResponse(RatingDto course, string result)
        {
            Course = course;
            Result = result;
        }

        public UpdateResponse(string result)
        {
            Result = result;
        }

        public UpdateResponse()
        {
        }
    }
}
