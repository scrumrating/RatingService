﻿using RatingService.Data.DTO;
using System;
using System.Collections.Generic;
using System.Text;

namespace RatingService.Consumers.Update
{
    public class UpdateCommand
    {
        public RatingDto Rating { get; set; }
    }
}
