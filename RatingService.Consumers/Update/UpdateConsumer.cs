﻿using MassTransit;
using RatingService.BI.ServiceInterfaces;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;

namespace RatingService.Consumers.Update
{
    public class UpdateConsumer : IConsumer<UpdateCommand>
    {
        private readonly IRatingService service;

        public UpdateConsumer(IRatingService service)
        {
            this.service = service;
        }

        public async Task Consume(ConsumeContext<UpdateCommand> context)
        {
            var item = service.Update(context.Message.Rating);
            if (item)
                await context.RespondAsync(new UpdateResponse(ConsumerConsts.success));
            else
                await context.RespondAsync(new UpdateResponse(ConsumerConsts.notfound));
        }
    }
}
