﻿using RatingService.Data.DTO;
using System;
using System.Collections.Generic;
using System.Text;

namespace RatingService.Consumers.GetRatingStudentList
{
    public class GetRatingStudentListResponse
    {
        public List<RatingStudentDto> RatingStudent { get; set; }
        public string Result { get; set; }

        public GetRatingStudentListResponse(List<RatingStudentDto> ratingStudent, string result)
        {
            RatingStudent = ratingStudent;
            Result = result;
        }

        public GetRatingStudentListResponse(string result)
        {
            Result = result;
        }

        public GetRatingStudentListResponse()
        {
        }
    }
}
