﻿using MassTransit;
using RatingService.BI.ServiceInterfaces;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;

namespace RatingService.Consumers.GetRatingStudentList
{
    public class GetRatingStudentListConsumer : IConsumer<GetRatingStudentListCommand>
    {
        private readonly IRatingService service;

        public GetRatingStudentListConsumer(IRatingService service)
        {
            this.service = service;
        }

        public async Task Consume(ConsumeContext<GetRatingStudentListCommand> context)
        {
            var list = service.GetRatingStudentList(context.Message.StrudentList,
                                                    context.Message.LessonId,
                                                    context.Message.StrudentList
                                                    );
                                
            if (list!=null)
                await context.RespondAsync(new GetRatingStudentListResponse(list,ConsumerConsts.success));
            else
                await context.RespondAsync(new GetRatingStudentListResponse(ConsumerConsts.notfound));
        }
    }
}
