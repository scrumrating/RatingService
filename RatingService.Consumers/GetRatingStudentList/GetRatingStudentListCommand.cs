﻿using RatingService.Data.DTO;
using System;
using System.Collections.Generic;
using System.Text;

namespace RatingService.Consumers.GetRatingStudentList
{
    public class GetRatingStudentListCommand
    {        
        
        public int[] StrudentList { get; set; }
        public int LessonId { get; set; }
        public int[] LomeWorkList { get; set; }

        public GetRatingStudentListCommand(int[] strudentList, int lessonId, int[] lomeWorkList)
        {
            StrudentList = strudentList;
            LessonId = lessonId;
            LomeWorkList = lomeWorkList;
        }
    }
}
