﻿using MassTransit;
using RatingService.BI.ServiceInterfaces;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;

namespace RatingService.Consumers.Delete
{
    public class DeleteConsumer : IConsumer<DeleteCommand>
    {
        private readonly IRatingService service;

        public DeleteConsumer(IRatingService service)
        {
            this.service = service;
        }

        public async Task Consume(ConsumeContext<DeleteCommand> context)
        {
            var item = service.Delete(context.Message.Id);
            if (item)
                await context.RespondAsync(new DeleteResponse(ConsumerConsts.success));
            else
                await context.RespondAsync(new DeleteResponse(ConsumerConsts.notfound));
        }
    }
}
