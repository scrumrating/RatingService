﻿using System;
using System.Collections.Generic;
using System.Text;

namespace RatingService.Consumers.Delete
{
    public class DeleteCommand
    {
        public int Id { get; set; }
    }
}
