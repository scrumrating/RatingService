﻿using RatingService.Data.DTO;
using System;
using System.Collections.Generic;
using System.Text;

namespace RatingService.Consumers.GetSummaryTeamList
{
    public class GetSummaryTeamListCommand
    {
        public TeamDto[] TeamList { get; set; }
        public int[] LessonList { get; set; }
        public int[] HomeWorkList { get; set; }

        public GetSummaryTeamListCommand(TeamDto[] teamList, int[] lessonList, int[] homeWorkList)
        {
            TeamList = teamList;
            LessonList = lessonList;
            HomeWorkList = homeWorkList;
        }
    }
}
