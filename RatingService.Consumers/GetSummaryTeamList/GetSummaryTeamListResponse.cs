﻿using RatingService.Data.DTO;
using System;
using System.Collections.Generic;
using System.Text;

namespace RatingService.Consumers.GetSummaryTeamList
{
   public class GetSummaryTeamListResponse
    {
        public List<SummaryTeamDto> SummaryTeam { get; set; }
        public string Result { get; set; }

        public GetSummaryTeamListResponse(List<SummaryTeamDto> summaryTeam, string result)
        {
            SummaryTeam = summaryTeam;
            Result = result;
        }

        public GetSummaryTeamListResponse(string result)
        {
            Result = result;
        }

        public GetSummaryTeamListResponse()
        {
        }
    }
}