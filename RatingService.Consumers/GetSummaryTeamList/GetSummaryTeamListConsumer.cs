﻿using MassTransit;
using RatingService.BI.ServiceInterfaces;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;

namespace RatingService.Consumers.GetSummaryTeamList
{
     public class GetSummaryTeamListConsumer : IConsumer<GetSummaryTeamListCommand>
    {
        private readonly IRatingService service;

        public GetSummaryTeamListConsumer(IRatingService service)
        {
            this.service = service;
        }

        public async Task Consume(ConsumeContext<GetSummaryTeamListCommand> context)
        {
            var list = service.GetSummaryTeamList(context.Message.TeamList,
                                                    context.Message.LessonList,
                                                    context.Message.HomeWorkList
                                                    );

            if (list != null)
                await context.RespondAsync(new GetSummaryTeamListResponse(list, ConsumerConsts.success));
            else
                await context.RespondAsync(new GetSummaryTeamListResponse(ConsumerConsts.notfound));
        }
    }
}