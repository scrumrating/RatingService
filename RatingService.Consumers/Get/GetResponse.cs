﻿using RatingService.Data.DTO;
using System;
using System.Collections.Generic;
using System.Text;

namespace RatingService.Consumers.Get
{
    public class GetResponse
    {
        public RatingDto Rating { get; set; }
        public string Result { get; set; }

        public GetResponse(RatingDto course, string result)
        {
            Rating = course;
            Result = result;
        }

        public GetResponse(string result)
        {
            Result = result;
        }

        public GetResponse()
        {
        }
    }

   
}
