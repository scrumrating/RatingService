﻿using System;
using System.Collections.Generic;
using System.Text;

namespace RatingService.Consumers.Get
{
    public class GetCommand
    {
        public int Id { get; set; }
    }
}
