﻿using MassTransit;
using RatingService.BI.ServiceInterfaces;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;

namespace RatingService.Consumers.Get
{
    public class GetOneConsumer : IConsumer<GetCommand>
    {
        private readonly IRatingService service;
        public GetOneConsumer(IRatingService service)
        {
            this.service = service;
        }

        public async Task Consume(ConsumeContext<GetCommand> context)
        {
            var item = service.Get(context.Message.Id);
            if (item != null)
                await context.RespondAsync(new GetResponse(item, ConsumerConsts.success));
            else
                await context.RespondAsync(new GetResponse(ConsumerConsts.notfound));
        }
    }
}
