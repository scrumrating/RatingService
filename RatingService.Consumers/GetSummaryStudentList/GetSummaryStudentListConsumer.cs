﻿using MassTransit;
using RatingService.BI.ServiceInterfaces;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;

namespace RatingService.Consumers.GetSummaryStudentList
{
   public class GetSummaryStudentListConsumer : IConsumer<GetSummaryStudentListCommand>
    {
        private readonly IRatingService service;

        public GetSummaryStudentListConsumer(IRatingService service)
        {
            this.service = service;
        }

        public async Task Consume(ConsumeContext<GetSummaryStudentListCommand> context)
        {
            var list = service.GetSummaryStudentList(context.Message.StrudentList,
                                                    context.Message.LessonList,
                                                    context.Message.StrudentList
                                                    );

            if (list != null)
                await context.RespondAsync(new GetSummaryStudentListResponse(list, ConsumerConsts.success));
            else
                await context.RespondAsync(new GetSummaryStudentListResponse(ConsumerConsts.notfound));
        }
    }
}
