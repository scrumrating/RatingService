﻿using System;
using System.Collections.Generic;
using System.Text;

namespace RatingService.Consumers.GetSummaryStudentList
{
    public class GetSummaryStudentListCommand
    {
        public int[] StrudentList { get; set; }
        public int[] LessonList { get; set; }
        public int[] HomeWorkList { get; set; }

        public GetSummaryStudentListCommand(int[] strudentList, int[] lessonList, int[] homeWorkList)
        {
            StrudentList = strudentList;
            LessonList = lessonList;
            HomeWorkList = homeWorkList;
        }
    }
}
