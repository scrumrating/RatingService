﻿using RatingService.Data.DTO;
using System;
using System.Collections.Generic;
using System.Text;

namespace RatingService.Consumers.GetSummaryStudentList
{
    public class GetSummaryStudentListResponse
    {
        public List<SummaryStudentDto> SummaryStudent { get; set; }
        public string Result { get; set; }

        public GetSummaryStudentListResponse(List<SummaryStudentDto> summaryStudent, string result)
        {
            SummaryStudent = summaryStudent;
            Result = result;
        }

        public GetSummaryStudentListResponse(string result)
        {
            Result = result;
        }

        public GetSummaryStudentListResponse()
        {
        }
    }
}