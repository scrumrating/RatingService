﻿using System;
using System.Collections.Generic;
using System.Text;

namespace RatingService.Core.Structures
{
    /// <summary>
    /// Результат запроса.
    /// </summary>
    public static class ResponseResult
    {
        /// <summary>
        /// Задаёт успешный результат.
        /// </summary>
        public const string Success = "success";

        /// <summary>
        /// Задаёт без успешный результат.
        /// </summary>
        public const string NotSuccess = "not-found";
    }
}
