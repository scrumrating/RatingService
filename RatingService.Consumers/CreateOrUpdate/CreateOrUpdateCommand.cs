﻿using RatingService.Data.DTO;
using System;
using System.Collections.Generic;
using System.Text;

namespace RatingService.Consumers.CreateOrUpdate
{
    public class CreateOrUpdateCommand
    {
        public RatingDto Rating { get; set; }
    }
}
