﻿using MassTransit;
using RatingService.BI.ServiceInterfaces;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;

namespace RatingService.Consumers.CreateOrUpdate
{
  public class CreateOrUpdateConsumer : IConsumer<CreateOrUpdateCommand>
    {
        private readonly IRatingService service;

        public CreateOrUpdateConsumer(IRatingService service)
        {
            this.service = service;
        }

        public async Task Consume(ConsumeContext<CreateOrUpdateCommand> context)
        {
            var item = service.CreateOrUpdate(context.Message.Rating);
            if (item)
                await context.RespondAsync(new CreateOrUpdateResponse(ConsumerConsts.success));
            else
                await context.RespondAsync(new CreateOrUpdateResponse(ConsumerConsts.notfcreated));
        }
    }
}
