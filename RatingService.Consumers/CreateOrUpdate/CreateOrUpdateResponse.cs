﻿using System;
using System.Collections.Generic;
using System.Text;

namespace RatingService.Consumers.CreateOrUpdate
{
   public class CreateOrUpdateResponse
    {
        public string Result { get; set; }

        public CreateOrUpdateResponse(string result)
        {
            Result = result;
        }

        public CreateOrUpdateResponse()
        {
        }
    }
}
