﻿using System;
using System.Collections.Generic;
using System.Text;

namespace RatingService.Consumers.Create
{
    public class CreateResponse
    {
        public string Result { get; set; }

        public CreateResponse(string result)
        {
            Result = result;
        }

        public CreateResponse()
        {
        }
    }
}
