﻿using MassTransit;
using RatingService.BI.ServiceInterfaces;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;

namespace RatingService.Consumers.Create
{
    public class CreateConsumer : IConsumer<CreateCommand>
    {
        private readonly IRatingService service;

        public CreateConsumer(IRatingService service)
        {
            this.service = service;
        }

        public async Task Consume(ConsumeContext<CreateCommand> context)
        {
            var item = service.Create(context.Message.Rating);
            if (item)
                await context.RespondAsync(new CreateResponse(ConsumerConsts.success));
            else
                await context.RespondAsync(new CreateResponse(ConsumerConsts.notfcreated));
        }
    }
}
