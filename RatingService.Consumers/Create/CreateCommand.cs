﻿using RatingService.Data.DTO;
using System;
using System.Collections.Generic;
using System.Text;

namespace RatingService.Consumers.Create
{
    public class CreateCommand
    {
        public RatingDto Rating { get; set; }
    }
}
