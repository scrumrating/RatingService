﻿using MassTransit;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;

namespace RatingService.Consumers.Healthchecks
{
    /// <summary>
    /// Обработчик сообщения получения состояния службы.
    /// </summary>
    public class HealthcheckConsumer : IConsumer<HealthcheckCommand>
    {

        /// <summary>
        /// Инициализирует новый экземпляр класса <see cref="HealthcheckConsumer"/>.
        /// </summary>
        /// <param name="logger">Абстракция над сервисом журналирования.</param>
        public HealthcheckConsumer()
        {
            
        }

        /// <summary>
        /// Обработать сообщение.
        /// </summary>
        /// <param name="context">Контекст обработки сообщения.</param>
        /// <returns>Асинхронная операция <see cref="Task"/>.</returns>
        public async Task Consume(ConsumeContext<HealthcheckCommand> context)
        {   
            await context.RespondAsync(new HealthcheckResponse { Result = "success" });
        }
    }
}