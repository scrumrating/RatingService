﻿using FakeItEasy;
using MassTransit;
using NUnit.Framework;
using RatingService.Consumers.Healthchecks;
using System;
using System.Collections.Generic;
using System.Text;

namespace RatingService.Consumers.Tests
{
    public class HealthcheckConsumerTests
    {
        [Test]
        [Description("Проверка состояния сервиса проходит успешно")]
        public void CanCheckHealth()
        {
            
            ConsumeContext<HealthcheckCommand> context = A.Fake<ConsumeContext<HealthcheckCommand>>();
            A.CallTo(() => context.Message).Returns(new HealthcheckCommand());

            HealthcheckConsumer sut = new HealthcheckConsumer();
            sut.Consume(context).GetAwaiter().GetResult();

            A.CallTo(
                    () => context.RespondAsync(A<HealthcheckResponse>.That.Matches(o => o.Result == "success")))
                .MustHaveHappenedOnceExactly();
        }
    }
}

#pragma warning disable CS1591